"""

"""
import src.crypto_utils as crypto

import unittest as ut


class EncryptionTests(ut.TestCase):

    def test_aes(self):
        data = "alo di sta imaaa"
        key = "dkeofksl8492kdjr"

        print(crypto.encrypt(data, key))

    def test_3des(self):
        data = "alo di sta imaaa"
        key = "dkeofksl8492kdjr"

        print(crypto.encrypt(data, key, algo='3des'))

    def test_rsa(self):
        data = "alo di sta imaaa"
        key = crypto.generate_rsa_key_pair()

        print(crypto.encrypt(data, key.publickey(), algo='rsa'))


class DecryptionTest(ut.TestCase):

    def test_aes(self):
        data = "alo di sta imaaa"
        key = "dkeofksl8492kdjr"

        encrypted = crypto.encrypt(data, key)
        decrypted = crypto.decrypt(encrypted, key)

        print(data == str(decrypted, 'utf-8'))

    def test_3des(self):
        data = "alo di sta imaaa"
        key = "dkeofksl8492kdjr"

        encrypted = crypto.encrypt(data, key, algo='3des')
        decrypted = crypto.decrypt(encrypted, key, algo='3des')

        print(data == str(decrypted, 'utf-8'))

    def test_rsa(self):
        data = "alo di sta imaaa"
        key = crypto.generate_rsa_key_pair()

        encrypted = crypto.encrypt(data, key.publickey(), algo='rsa')
        decrypted = crypto.decrypt(encrypted, key, algo='rsa')

        print(data == str(decrypted, 'utf-8'))


class TestHashing():

    def test_sha_1(self):
        data = "alo mala di si sta ima"

        print(crypto._calculate_sha1(data))

    def test_sha_2(self):
        data = "alo mala di si sta ima"

        print(crypto._calculate_sha3(data, 256))
