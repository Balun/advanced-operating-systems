"""

"""

import abc


def print_element(file_name, element):
    """

    :param element:
    :return:
    """
    with open(file_name, 'w') as fp:
        fp.write("---BEGIN OS2 CRYPTO DATA---\n")
        fp.write(str(element))
        fp.write("---END OS2 CRYPTO DATA---\n")


class CrpytoElement(abc.ABC):
    """

    """

    def __init__(self, description, method):
        """

        :param description:
        :param Method:
        """
        self.description = description
        self.method = (method,)

    def __str__(self):
        if len(self.method) == 1:
            return "Description:\n    %s\n\n " \
                   "Method:\n    %s\n\n" % (self.description, self.method)

        else:
            builder = "Description:\n    %s\n\n" % self.method
            builder += "Method:\n"
            builder += "    %s\n" % self.method[0]
            builder += "    %s\n\n" % self.method[1]

            return builder


class CryptedFile(CrpytoElement):
    """

    """

    def __init__(self, description, method, file_name, data):
        """

        :param description:
        :param method:
        :param file_name:
        :param data:
        """
        super().__init__(description, method)

        self.file_name = file_name
        self.data = data

    def __str__(self):
        builder = super().__str__()
        builder += "File name:\n    %s\n\n" % self.file_name
        builder += "Data:\n"

        builder += _write_large_data(self.data)

        return builder


class SecretKey(CrpytoElement):
    """

    """

    def __init__(self, description, method, secret_key):
        """

        :param description:
        :param method:
        :param secret_key:
        """
        super().__init__(description, method)

        self.secret_key = secret_key

    def __str__(self):
        builder = super().__str__()

        builder += "Secret Key:\n    %s\n\n" % str(self.secret_key)

        return builder


class PrivateKey(CrpytoElement):
    """

    """

    def __init__(self, description, method, key_length, modulus,
                 private_exponent):
        """

        :param description:
        :param method:
        :param key_length:
        :param modulus:
        :param private_exponent:
        """
        super().__init__(description, method)

        self.key_length = key_length
        self.modulus = modulus
        self.private_exponent = private_exponent

    def __str__(self):
        builder = super().__str__()

        builder += "Key Length:\n    %s\n\n" % str(self.key_length)

        builder += "Modulus:\n"
        builder += _write_large_data(self.modulus)

        builder += "Private Exponent:\n"
        builder += _write_large_data(self.private_exponent)

        return builder


class PublicKey(CrpytoElement):
    """
    """

    def __init__(self, description, method, key_length, modulus,
                 public_exponent):
        """

        :param description:
        :param method:
        :param key_length:
        :param modulus:
        :param public_exponent:
        """
        super().__init__(description, method)

        self.key_length = key_length
        self.modulus = modulus
        self.public_exponent = public_exponent

    def __str__(self):
        builder = super().__str__()

        builder += "Key Length:\n    %s\n\n" % str(self.key_length)

        builder += "Modulus:\n"
        builder += _write_large_data(self.modulus)

        builder += "Public Exponent:\n"
        builder += _write_large_data(self.public_exponent)

        return builder


class DigitalEnvelope(CrpytoElement):
    """

    """

    def __init__(self, description, file_name, method, key_length,
                 envelope_data, envelope_crypt_key):
        super().__init__(description, method)

        self.file_name = file_name
        self.key_length = key_length
        self.envelope_data = envelope_data
        self.envelope_crypt_key = envelope_crypt_key

    def __str__(self):
        builder = super().__str__()

        builder += "File name:\n    %s\n\n" % self.file_name

        builder += "Key length:\n"
        builder += "    %s\n" % str(self.key_length[0])
        builder += "    %s\n\n" % str(self.key_length[1])

        builder += "Envelope data:\n"
        builder += _write_large_data(self.envelope_data)

        builder += "Envelope crypt key:\n"
        builder += _write_large_data(self.envelope_crypt_key)

        return builder


class DigitalSignature(CrpytoElement):
    """

    """

    def __init__(self, description, file_name, method, key_length, signature):
        """

        :param description:
        :param file_name:
        :param method:
        :param key_length:
        :param signature:
        """
        super().__init__(description, method)

        self.file_name = file_name
        self.key_length = key_length
        self.signature = signature

    def __str__(self):
        builder = super().__str__()

        builder += "File name:\n    %s\n" % self.file_name

        builder += "Key length:\n"
        builder += "    %s\n" % self.key_length[0]
        builder += "    %s\n\n" % self.key_length[1]

        builder += "Signature:\n"
        builder += _write_large_data(self.signature)


class DigitalSeal(CrpytoElement):
    """

    """

    def __init__(self, description, file_name, method, key_length, signature):
        """

        :param description:
        :param file_name:
        :param method:
        :param key_length:
        :param signature:
        """
        super().__init__(description, method)

        self.file_name = file_name
        self.key_length = key_length
        self.signature = signature

    def __str__(self):
        builder = super().__str__()

        builder += "File name:\n    %s\n" % self.file_name

        builder += "Key length:\n"
        builder += "    %s\n" % self.key_length[0]
        builder += "    %s\n\n" % self.key_length[1]

        builder += "Signature:\n"
        builder += _write_large_data(self.signature)


def _write_large_data(data):
    """

    :param data:
    :return:
    """
    builder = "    "

    for i, char in enumerate(data, 1):
        builder += str(char)

        if i % 60 == 0:
            builder += "\n    "

    builder += "\n\n"

    return builder
