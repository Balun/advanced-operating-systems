"""

"""

import Crypto.Cipher.AES as aes
import Crypto.Cipher.DES3 as des3
import Crypto.Random as random
import Crypto.PublicKey.RSA as rsa
import Crypto.Cipher.PKCS1_OAEP as oaep

import hashlib
import ast

ENC_MODE = {'ecb': aes.MODE_ECB, 'cbc': aes.MODE_CBC, 'cfb': aes.MODE_CFB,
            'ofb': aes.MODE_OFB, 'ctr': aes.MODE_CTR}


def encrypt(data, key, mode='ecb', algo='aes'):
    """

    :param data:
    :param algo:
    :return:
    """
    if algo == 'aes':
        return _encrypt_aes(data, key, mode)

    elif algo == '3des':
        return _encrypt_3des(data, key, mode)

    elif algo == 'rsa':
        return _encrypt_rsa(data, key)

    else:
        raise ValueError("Unknown algorithm specified.")


def decrypt(data, key, mode='ecb', algo='aes'):
    """

    :param data:
    :param key:
    :param mode:
    :param algo:
    :param nonce:
    :param tag:
    :return:
    """
    if algo == 'aes':
        return _decrypt_aes(data, key, mode)

    elif algo == '3des':
        return _decrypt_3des(data, key, mode)

    elif algo == 'rsa':
        return _decrypt_rsa(data, key)

    else:
        raise ValueError("Unknown algorithm specified.")


def calculate_hash(data, mode=256, algo='sha'):
    """

    :param data:
    :param mode:
    :param algo:
    :return:
    """
    if mode == 1:
        return _calculate_sha1(data)

    elif mode > 1:
        return _calculate_sha3(data, mode)

    else:
        raise ValueError("Unknown algorithm specified.")


def generate_rsa_key_pair(size=1024):
    """

    :param size:
    :return:
    """
    random_generator = random.new().read
    return rsa.generate(size, random_generator)  # generate pub and priv key


def align_to_block(data, block_size=16):
    if len(data) < 16:
        while len(data) < 16:
            data = data + " "

    return data


def _encrypt_aes(data, key, mode):
    """

    :param data:
    :param key:
    :param mode:
    :return:
    """
    try:
        key = key.encode('ascii')
    except AttributeError:
        pass

    if len(key) not in (16, 24, 32):
        raise ValueError("Illegal key length.")

    cipher = aes.new(key, ENC_MODE[mode.lower()])

    ciphertext = cipher.encrypt(data.encode('utf-8'))

    return ciphertext


def _encrypt_3des(data, key, mode):
    """

    :param data:
    :param key:
    :param mode:
    :return:
    """
    key = des3.adjust_key_parity(key.encode('ascii'))

    cipher = des3.new(key, ENC_MODE[mode.lower()])
    return cipher.encrypt(data.encode('utf-8'))


def _encrypt_rsa(data, key):
    """

    :param data:
    :param key:
    :return:
    """
    encryptor = oaep.new(key)
    try:
        data = data.encode('utf-8')
    except AttributeError:
        pass

    return encryptor.encrypt(data)


def _decrypt_aes(data, key, mode):
    """

    :param data:
    :param key:
    :param mode:
    :return:
    """
    try:
        key = key.encode('ascii')
    except AttributeError:
        pass

    try:
        data = data.encode('utf-8')
    except AttributeError:
        pass

    if len(key) not in (16, 24, 32):
        raise ValueError("Illegal key length.")

    cipher = aes.new(key, ENC_MODE[mode.lower()])

    return cipher.decrypt(data)


def _decrypt_3des(data, key, mode):
    """

    :param data:
    :param key:
    :param mode:
    :return:
    """
    key = des3.adjust_key_parity(key.encode('ascii'))
    try:
        data = data.encode('utf-8')
    except AttributeError:
        pass

    cipher = des3.new(key, ENC_MODE[mode.lower()])
    return cipher.decrypt(data)


def _decrypt_rsa(data, key):
    """

    :param data:
    :param key:
    :return:
    """
    decryptor = oaep.new(key)
    return decryptor.decrypt(ast.literal_eval(str(data)))


def _calculate_sha1(data):
    """

    :param data:
    :return:
    """
    sha = hashlib.sha1()
    sha.update(data.encode('utf-8'))

    return sha.digest()


def _calculate_sha3(data, mode):
    """

    :param data:
    :param mode:
    :return:
    """
    if mode == 224:
        sha = hashlib.sha224()

    elif mode == 256:
        sha = hashlib.sha256()

    elif mode == 384:
        sha = hashlib.sha384()

    elif mode == 512:
        sha = hashlib.sha512()

    else:
        raise ValueError("Unknown mode specified.")

    try:
        data = data.encode('utf-8')
    except AttributeError:
        pass

    sha.update(data)

    return sha.digest()
