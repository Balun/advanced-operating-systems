#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import sys

import Crypto.Random as random

import src.crypto_utils as crypto
import src.io_utils as io


def generate_signature(hash_algo='sha-256'):
    """

    :param hash_algo:
    :return:
    """
    algo, mode = hash_algo.split("-")

    data = input("Specify data to digest: ")
    data = crypto.align_to_block(data)
    digest = crypto.calculate_hash(data, int(mode), algo)

    # Ana dobiva ključ
    key_a = crypto.generate_rsa_key_pair()

    # Ana šalje sažetu poruku kriptiranu privatnim ključem
    message = crypto.encrypt(digest, key_a, algo='rsa')

    # Brano dekriptira poruku Aninim javnim ključem
    decrypted = crypto.decrypt(message, key_a, algo='rsa')

    digest_check = crypto.calculate_hash(data, int(mode), algo)

    if digest_check == decrypted:
        print("signature procedure finished successfully")
    else:
        print("Error in digital signature procedure.")


def generate_envelope(sim_algo='aes', sim_mode='ecb', key_size=128):
    """

    :param sim_algo:
    :return:
    """
    # Generiranje brankovih ključeva
    key_b = crypto.generate_rsa_key_pair()

    # Generiranje tajnog simetričnog ključa
    secret_key = random.get_random_bytes(int(key_size / 8))

    # unos podataka
    data = input("Specify data to digest: ")
    data = crypto.align_to_block(data)

    # simetrično enkriptiranje podataka tajnim ključem
    encrypted = crypto.encrypt(data, secret_key, sim_mode, algo=sim_algo)

    # enkriptiranje tajnog ključa brankovim javnim ključem
    encrypted_key = crypto.encrypt(secret_key, key_b.publickey(), algo='rsa')

    message = (encrypted, encrypted_key)

    # dekriptiranje tajnog ključa brankovim privatnim ključem
    decrypted_key = crypto.decrypt(message[1], key_b, sim_mode, algo='rsa')

    # dekriptiranje podataka dekriptiranim tajnim ključem
    decrypted_text = crypto.decrypt(message[0], decrypted_key, sim_mode,
                                    algo=sim_algo)

    print("Dekriptirani tekst:", str(decrypted_text, 'utf-8'))


def generate_seal(sim_algo='aes', sim_mode='ecb', hash_algo='sha-256',
                  key_size=128):
    """

    :param sim_algo:
    :return:
    """
    hash_algo = hash_algo if hash_algo else 'sha-256'
    hash_algo, hash_mode = hash_algo.split("-")

    # Generiranje tajnog simetričnog ključa
    secret_key = random.get_random_bytes(int(key_size / 8))

    # Generiranje asimetričnih ključeva
    key_a = crypto.generate_rsa_key_pair()
    key_b = crypto.generate_rsa_key_pair()

    # unos podataka
    data = input("Specify data to digest: ")
    data = crypto.align_to_block(data)

    # kriptiranje teksta tajnim ključem
    crypted_text = crypto.encrypt(data, secret_key, sim_mode, algo=sim_algo)

    # Kriptiranje tajnog ključa Brankovim javnim ključem
    crypted_key = crypto.encrypt(secret_key, key_b.publickey(), algo='rsa')

    # sažetak kriptiranog teksta i kriptiranog tajnog ključa
    digest = crypto.calculate_hash(crypted_text + crypted_key, int(hash_mode),
                                   hash_algo)

    # kriptirani sažetak
    crypted_digest = crypto.encrypt(digest, key_a, algo='rsa')

    # poruka od tri komponente se šalje branku
    message = (crypted_text, crypted_key, crypted_digest)

    # Branko dekriptira tajni ključ svojim privatnim ključem
    decrypted_key = crypto.decrypt(message[1], key_b, algo='rsa')

    # Branko dekriptira tekst dobivenim tajnim ključem
    decrypted_text = crypto.decrypt(message[0], decrypted_key, sim_mode,
                                    algo=sim_algo)

    # Branko dekriptira sažetak poruke Aninim javnim ključem
    decrypted_digest = crypto.decrypt(message[2], key_a,
                                      algo='rsa')

    # Branko izračunava sažetak dobivenog teksa
    message_digest = crypto.calculate_hash(message[0] + message[1],
                                           int(hash_mode),
                                           hash_algo)

    print("Dobivena poruka:", decrypted_text)
    print("Sažeci poruka se poklapaju:", message_digest == decrypted_digest)

    io.print_element("seal.txt")


def get_desc():
    """

    :return:
    """
    desc = "Welcome to Cryptipy, an example program for generating" \
           "digital signature, envelope and seal.\n"
    desc += "Written by univ. bacc. ing. comp. Matej Balun, University of " \
            "Zagreb, Faculty of Electrical Engineering and Computing.\n"
    desc += "You can read the instructions using \"./cryptipy.py -h\".\n"

    return desc


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=get_desc())
    subparsers = parser.add_subparsers(help='mode', dest='mode')

    # Digital signature
    signature_parser = subparsers.add_parser('signature',
                                             help="Generate a digital signature.")

    signature_parser.add_argument('--hash-algo', help='Specify hashing '
                                                      'algorithm, default is SHA-256')

    # Digital envelope
    envelope_parser = subparsers.add_parser('envelope', help='Generate a '
                                                             'digital '
                                                             'envelope')

    envelope_parser.add_argument('--sim-algo',
                                 help='Specify simetric crypto algorithm. Default is AES')

    envelope_parser.add_argument('--sim_mode',
                                 help='Specify mode for symetric algorithm. '
                                      'Default is ECB')

    envelope_parser.add_argument('--key-size',
                                 help='Specify key size symetric algorithm. '
                                      'Default is 128')

    # Digital seal
    seal_parser = subparsers.add_parser('seal', help='Generate a digital '
                                                     'seal.')

    seal_parser.add_argument('--sim-algo',
                             help='Specify simetric algorithm. Default is AES')

    seal_parser.add_argument('--sim-mode',
                             help='Specify mode for symetric algorithm. '
                                  'Default is ECB')

    seal_parser.add_argument('--hash-algo', help='Specify hashing '
                                                 'algorithm, default is SHA-256')

    seal_parser.add_argument('--key-size',
                             help='Specify key size symetric algorithm. '
                                  'Default is 128')

    args = parser.parse_args()
    mode = args.mode

    args = {key.replace('-', '_'): value for key, value in args.__dict__.items(
    ) if (value and key != 'mode')}

    if mode == 'signature':
        generate_signature(**args)
    elif mode == 'envelope':
        generate_envelope(**args)
    elif mode == 'seal':
        generate_seal(**args)
    else:
        sys.stderr.write("Unknown mode specified.")
