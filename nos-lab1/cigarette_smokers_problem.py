import multiprocessing as mp
import random
import time


def tobacco_smoker(queue):
    while True:

        items = queue.get()

        if all(i in items for i in ["PAPER", "MATCHES"]):
            print("Pušač sa Duhanom puši cigaretu...")
            time.sleep(2)
            print("Stol slobodan")
            queue.put(["FINISHED"])

        else:
            queue.put(items)


def paper_smoker(queue):
    while True:
        items = queue.get()

        if all(i in items for i in ["TOBACCO", "MATCHES"]):
            print("Pušač sa rizlama puši cigaretu...")
            time.sleep(2)
            print("Stol slobodan")
            queue.put(["FINISHED"])

        else:
            queue.put(items)


def matches_smoker(queue):
    while True:
        items = queue.get()

        if all(i in items for i in ["PAPER", "TOBACCO"]):
            print("Pušač sa Šibicama puši cigaretu...")
            time.sleep(2)
            print("Stol slobodan")
            queue.put(["FINISHED"])

        else:
            queue.put(items)


def vendor():
    queue = mp.Queue()

    ingredients = ["TOBACCO", "PAPER", "MATCHES"]

    tobacco = mp.Process(target=tobacco_smoker, args=(queue,), name="Tobacco "
                                                                    "smoker")
    paper = mp.Process(target=paper_smoker, args=(queue,), name="Paper smoker")
    matches = mp.Process(target=matches_smoker, args=(queue,), name="Matches "
                                                                    "Smoker")

    tobacco.start()
    paper.start()
    matches.start()

    queue.put("FINISHED")

    while True:
        items = queue.get()

        if "FINISHED" in items:
            items = random.sample(ingredients, 2)
            print("print na stolu su:", items)
            time.sleep(2)
            queue.put(items)
        else:
            queue.put(items)
            time.sleep(2)


if __name__ == '__main__':
    vendor = mp.Process(target=vendor, name="Vendor")
    vendor.start()
