import multiprocessing as mp
import sys
import time

WRITE = 1
READ = 0


def philosopher_task(id, pipes, count):
    clock = id + 1

    queue = count * [sys.maxsize]
    responses = count * [0]

    pipes[id][WRITE].close()
    for i, p in enumerate(pipes):
        if i != id:
            p[READ].close()

    time.sleep(1)

    # TODO
    pass


if __name__ == '__main__':
    count = int(sys.argv[1])

    pipes = [mp.Pipe() for _ in range(count)]
    philosophers = [mp.Process(target=philosopher_task, args=(i, pipes,
                                                              count)) for
                    i in range(count)]

    for p in philosophers:
        p.start()

    for p in philosophers:
        p.join()
